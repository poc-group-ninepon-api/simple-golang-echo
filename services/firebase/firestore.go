package firebase

import (
	"context"
	"fmt"
	"ninepon-golang-echo/services"

	firestore "cloud.google.com/go/firestore"
)

type Firestore struct {
	Client *firestore.Client
}

// initialiing the firestore
func ConnectFirestore() Firestore {
	firebase := services.NewFirebaseApp()
	ctx := context.Background()
	clientConn, err := firebase.App.Firestore(ctx)
	if err != nil {
		fmt.Println("Unable to initialize firestore: ", err)
	}
	return Firestore{Client: clientConn}
}
