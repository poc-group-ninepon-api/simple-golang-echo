package mysql

import (
	"fmt"

	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectLocalhost() *gorm.DB {
	dbUser := viper.GetString("mysql.username")
	dbPassword := viper.GetString("mysql.password")
	dbHost := viper.GetString("mysql.host")
	dbPort := viper.GetString("mysql.port")
	dbDBName := viper.GetString("mysql.dbname")
	dbCharset := viper.GetString("mysql.charset")
	dbParseTime := viper.GetString("mysql.parseTime")
	dbLoc := viper.GetString("mysql.loc")

	stringConnectDB := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=%s&loc=%s", dbUser, dbPassword, dbHost, dbPort, dbDBName, dbCharset, dbParseTime, dbLoc)
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       stringConnectDB, // data source name
		DefaultStringSize:         256,             // default size for string fields
		DisableDatetimePrecision:  true,            // disable datetime precision, which not supported before MySQL 5.6
		DontSupportRenameIndex:    true,            // drop & create when rename index, rename index not supported before MySQL 5.7, MariaDB
		DontSupportRenameColumn:   true,            // `change` when rename column, rename column not supported before MySQL 8, MariaDB
		SkipInitializeWithVersion: false,           // auto configure based on currently MySQL version
	}), &gorm.Config{})

	if err != nil {
		return nil
	}

	return db
}
