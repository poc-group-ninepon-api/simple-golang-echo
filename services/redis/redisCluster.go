package redis

import (
	"fmt"

	"github.com/go-redis/redis"
	"github.com/spf13/viper"
)

var RedisConnection *redis.Client

func ConnectPubsub() *redis.Client {
	redisService := fmt.Sprint(viper.GetString("redis.host"), ": ", viper.GetInt("redis.port"))
	redisPassword := viper.GetString("redis.password")
	rdbConn := redis.NewClient(&redis.Options{
		Addr:     redisService,
		Password: redisPassword,
		// DB:       0, // use default DB
		// ReadTimeout:  -1,
		// WriteTimeout: -1,
	})

	return rdbConn
}
