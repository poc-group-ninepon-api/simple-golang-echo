package services

import (
	"context"
	"log"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

var (
	projectid string = "gitlab-ninepon-poc"
)

type Firebaseapp struct {
	App *firebase.App
}

// initialize firebase application in my project
func NewFirebaseApp() *Firebaseapp {
	ctx := context.Background()
	conf := &firebase.Config{ProjectID: projectid}
	opt := option.WithCredentialsFile("./config/serviceAccountKey.json")
	app, err := firebase.NewApp(ctx, conf, opt)
	if err != nil {
		log.Fatalln(err)
	}
	return &Firebaseapp{
		App: app,
	}
}
