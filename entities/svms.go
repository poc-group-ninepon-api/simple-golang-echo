package entities

type SVMSLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ErrorMessage struct {
	Message string `json:"message"`
}

type Result struct {
	Data string `json:"data"`
}
