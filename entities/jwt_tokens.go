package entities

import "time"

type JwtTokens struct {
	ID           int       `json:"id" gorm:"primary_key;type:int(11);unique;not null"`
	Username     string    `json:"username" gorm:"ForeignKey:Users(username)"`
	Type         string    `josn:"type" gorm:"type:varchar(100);not null"`
	Token        string    `josn:"token" gorm:"type:text;not null"`
	RefreshToken string    `josn:"refresh_token" gorm:"type:text;not null"`
	CreatedAt    time.Time `gorm:"type:datetime;"`
	UpdatedAt    time.Time `gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
