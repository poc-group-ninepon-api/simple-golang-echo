package entities

import "time"

type Users struct {
	ID           int       `json:"id" gorm:"primary_key;type:int(11);unique;not null"`
	IDcode       string    `json:"idcode" gorm:"type:varchar(18);not null" validate:"required,alphanum,min=8"`
	Fullname     string    `json:"fullname" gorm:"type:text;not null" validate:"required,min=4"`
	Tel          string    `json:"tel" gorm:"type:varchar(20);unique;not null" validate:"required,min=2"`
	Type_account string    `json:"type_account" gorm:"type:enum('admin','user');not null" validate:"required,oneof=admin user"`
	Username     string    `json:"username" gorm:"type:varchar(20);unique;not null"`
	Password     string    `json:"password,omitempty" gorm:"type:text;not null"`
	CreatedAt    time.Time `json:"created_at" gorm:"type:datetime;"`
	UpdatedAt    time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
