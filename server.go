package main

import (
	"ninepon-golang-echo/config"
	"ninepon-golang-echo/helper"
	"ninepon-golang-echo/start"
	"ninepon-golang-echo/tasks"
	"ninepon-golang-echo/providers"
)

func main() {
	helper.GoRoutineAndChannelExample()
	tasks.TaskSample()

	//NOTE load ENV
	config.LoadConfig()

	providers.SVMSLogin("test-admin", "1234567890")

	//NOTE start route
	start.Router()
}
