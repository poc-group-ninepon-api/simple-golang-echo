package helper

import (
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func RandomUUID() string {
	id := uuid.New()
	return id.String()
}

func HashPassword(password string) (string, error) {
    bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
    return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
    err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
    return err == nil
}