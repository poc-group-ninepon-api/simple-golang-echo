package helper

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/spf13/viper"
)

type Claims struct {
	Username   string `json:"username"`
	Device     string `json:"device"`
	DeviceType string `json:"deviceType"`
	Platform   string `json:"platform"`
	jwt.RegisteredClaims
}

type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

func GenerateJWT(userPwd map[string]string) string {
	var jwtKey = []byte(viper.GetString("app.jwtKey"))
	expirationTime := time.Now().Add(24 * time.Hour) //.Add(1 * time.Second) //Add(5 * time.Minute)

	claims := &Claims{
		Username:   userPwd["username"],
		Device:     "XX-PC-00",
		DeviceType: "PC",
		Platform:   "NINEPON",
		RegisteredClaims: jwt.RegisteredClaims{
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return ""
	}

	return tokenString
}

func ValidatePreviousToke(tokenPrevious string) string {
	token, err := jwt.Parse(tokenPrevious, func(token *jwt.Token) (interface{}, error) {
		return []byte("AllYourBase"), nil
	})
	if token.Valid {
		//fmt.Println("You look nice today")
		return tokenPrevious
	} else if errors.Is(err, jwt.ErrTokenMalformed) {
		// fmt.Println("That's not even a token")
		return ""
	} else if errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet) {
		// Token is either expired or not active yet
		// fmt.Println("Timing is everything")
		return ""
	} else {
		// fmt.Println("Couldn't handle this token:", err)
		return ""
	}
}
