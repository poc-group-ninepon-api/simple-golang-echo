package helper

import (
	"fmt"
)

func GoRoutineAndChannelExample() {
	Routine()
}

func Routine() {

	fmt.Println("Start function GoRoutine")

	//NOTE declear variable chan to string
	result := make(chan string)
	name := "Fake name"
	go Channel01(name, result)
	fmt.Println(<-result)
}

func Channel01(name string, result chan<- string) {
	fmt.Println("Start function GoChannel01")
	output := "Hello " + name
	// fmt.Printf("In GoChannel = %s\n", output)
	result <- output
}
