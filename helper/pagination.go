package helper

import (
	"math"
	"net/url"
	"strconv"

	"ninepon-golang-echo/entities"
)

func SetByParamQuery(cURL url.Values) map[string]int {
	params := cURL
	var offset int = 0
	currentPage, currentPageErr := strconv.Atoi(params.Get("currentPage"))
	perPage, perPageErr := strconv.Atoi(params.Get("perPage"))
	if currentPageErr != nil || perPageErr != nil {
		currentPage = 1
		perPage = 0
	} else {
		offset = (currentPage - 1) * perPage
	}
	res := map[string]int{
		"currentPage": currentPage,
		"perPage":     perPage,
		"offset":      offset,
	}

	return res
}

func ResponseWithPagination(totalRow int, currentPage int, perPage int, queryUsers []entities.Users) map[string]interface{} {
	lastPage := int(math.Ceil(float64(totalRow) / float64(perPage)))

	res := map[string]interface{}{
		"currentPage": currentPage,
		"lastPage":    lastPage,
		"total":       totalRow,
		"count":       len(queryUsers),
		"data":        queryUsers,
	}
	return res
}
