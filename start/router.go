package start

import (
	"fmt"
	"net/http"

	"ninepon-golang-echo/config"
	"ninepon-golang-echo/controllers"
	"ninepon-golang-echo/migrations"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

type MyValidator struct{}

func (cv *MyValidator) Validate(i interface{}) error {
	return nil
}

func InitRoute() *echo.Echo {
	e := echo.New()
	e.Debug = true
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	e.Use(ServerHeader)
	return e
}

func Router() {
	e := echo.New()
	e.Debug = true
	e.Use(middleware.CORSWithConfig(config.ConfigCors()))
	e.Validator = &MyValidator{}

	//NOTE use middleware in route group instead of below code
	// e.Use(ServerHeader)

	//NOTE only route admin login and user login don't need to use middelware header
	e.POST("/user/login", controllers.UserLogin)
	e.POST("/admin/login", controllers.AdminLogin)

	groupAdmin := e.Group("/admin", ServerHeader)
	{
		groupAdmin.GET("/hello", func(c echo.Context) error {
			return c.JSON(http.StatusOK, "hello world!")
		})
	}

	groupUser := e.Group("/user", ServerHeader)
	{
		groupUser.GET("/:username", controllers.UserShow)
		groupUser.GET("/index", controllers.UserIndex)
	}

	groupRedis := e.Group("/redis")
	{
		groupRedis.GET("/:key", controllers.ReadRedisKey)
		groupRedis.POST("/update", controllers.WriteRedisKey)
	}

	groupMysql := e.Group("/mysql")
	{
		groupMysql.GET("", controllers.MySQLIndex)
		groupMysql.POST("", controllers.MysqlStore)
		groupMysql.GET("/:userIcode", controllers.MySQLShow)
		groupMysql.PUT("/:userIcode", controllers.MysqlUpdate)
		groupMysql.DELETE("/:userIcode", controllers.MysqlDelete)

		groupMysql.POST("/migration", migrations.DBMigration)
	}

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.POST("/users", func(c echo.Context) error {
		name := c.QueryParam("name")
		fmt.Println("name ===> " + name)
		return c.String(http.StatusOK, name)
	})
	fmt.Println(viper.GetString("PORT"))
	e.Logger.Fatal(e.Start(":" + viper.GetString("app.port")))

}
