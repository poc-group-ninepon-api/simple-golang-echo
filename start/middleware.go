package start

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

type User struct {
	Name  string `json:"name" xml:"name"`
	Email string `json:"email" xml:"email"`
}

func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	res := func(c echo.Context) error {
		// fmt.Println(c.Request().Header)
		// fmt.Println(c.Request().Header["Authorization"])
		if val, exists := c.Request().Header["Authorization"]; exists {
			fmt.Println("Auth ==>", val)
			c.Response().Header().Set(echo.HeaderServer, "Echo/3.0")
			return next(c)
		} else {
			//NOTE 401 (unautherization)
			return c.NoContent(http.StatusUnauthorized)
		}
	}
	return res
}
