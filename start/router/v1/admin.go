package route_v1_admin

import (
	"fmt"
	"net/http"
	"ninepon-golang-echo/helper"
	"ninepon-golang-echo/start"

	"github.com/labstack/echo"
)

type UsernamePassword struct {
	Username string `json:"username" form:"username" query:"username"`
	Password string `json:"password" form:"password" query:"password"`
}

type tokenResponse struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
}

func GroupAdminV1Router() {
	initRoute := start.InitRoute()
	groupAdmin := initRoute.Group("/v1/admin")
	groupAdmin.POST("/login", loginAdmin)
}

func loginAdmin(c echo.Context) error {
	userpwd := new(UsernamePassword)
	if err := c.Bind(userpwd); err != nil {
		//NOTE if format not equal in type structure
		return c.String(http.StatusBadRequest, "bad request")
	}
	fmt.Println(userpwd.Username)
	fmt.Println(userpwd.Password)
	userPwd := map[string]string{
		"username": userpwd.Username,
		"password": userpwd.Password,
	}
	tokenJWT := helper.GenerateJWT(userPwd)
	if len(tokenJWT) <= 0 {
		return c.NoContent(http.StatusInternalServerError)
	}
	tokenResponse := &tokenResponse{
		Token:        tokenJWT,
		RefreshToken: helper.RandomUUID(),
	}
	return c.JSON(http.StatusOK, tokenResponse)
}
