package providers

import (
	"fmt"
	"time"
	"encoding/json"

	"ninepon-golang-echo/entities"

	"github.com/imroc/req/v3"
	"github.com/spf13/viper"
)

func SVMSLogin(username string, password string) {
	var errMsg entities.ErrorMessage
	svmsLogin := &entities.SVMSLogin{Username: username, Password: password}
	endpoint := viper.GetString("SVMS.endpoint")
	fmt.Println("endpoint: " + endpoint)

	client := req.C().
		SetTimeout(10 * time.Second)

	resp, err := client.R().
		SetHeader("Accept", "*/*"). // Chainable request settings.
		SetHeader("Content-Type", "application/json").
		SetBody(svmsLogin).
		// SetPathParam("username", "imroc"). // Replace path variable in url.
		// SetResult(&entities.Result). // Unmarshal response body into userInfo automatically if status code is between 200 and 299.
		SetError(&errMsg). // Unmarshal response body into errMsg automatically if status code >= 400.
		// EnableDump(). // Enable dump at request level, only print dump content if there is an error or some unknown situation occurs to help troubleshoot.
		Post(endpoint + "/login")

	if err != nil { // Error handling.
		fmt.Println("error:", err)
		fmt.Println("raw content:")
		fmt.Println(resp.Dump()) // Record raw content when error occurs.
		return
	}

	if resp.IsError() { // Status code >= 400.
		fmt.Println(errMsg.Message) // Record error message returned.
		return
	}

	if resp.IsSuccess() { // Status code is between 200 and 299.
		// fmt.Printf("%s (%s)\n", userInfo.Name, userInfo.Blog)
		jsonString := resp
		var data map[string]interface{}
		json.Unmarshal([]byte(jsonString.String()), &data)
		fmt.Println(data["token"])
		return
	}
}
