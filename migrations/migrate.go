package migrations

import (
	"fmt"
	"net/http"

	"ninepon-golang-echo/entities"
	"ninepon-golang-echo/services/mysql"

	"github.com/labstack/echo"
)

func DBMigration(c echo.Context) error {
	// connect database
	db := mysql.ConnectLocalhost()
	if db == nil {
		fmt.Println("error: connect database error")
	}
	sqlDB, err := db.DB()
	if err != nil {
		fmt.Println("error: database function DB error")
	}
	defer sqlDB.Close()

	db.Migrator().CurrentDatabase()
	migration := db.Set("gorm:table_options", "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4").Migrator()

	//table users
	migration.CreateTable(&entities.Users{})

	//table jwt_tokens
	migration.CreateTable(&entities.JwtTokens{})

	return c.JSON(http.StatusOK, "TestMigrate!!")
}
