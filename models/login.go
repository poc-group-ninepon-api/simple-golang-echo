package models

import (
	"net/http"

	"ninepon-golang-echo/entities"
	"ninepon-golang-echo/helper"
	"ninepon-golang-echo/services/mysql"
	"ninepon-golang-echo/validators"
)

func GetDataUserLogin(reqLogin *validators.Login, accountType string) (int, string, []entities.Users) {
	db := mysql.ConnectLocalhost()
	if db == nil {
		return http.StatusBadGateway, "can't connect to database", nil
	}
	sqlDB, err := db.DB()
	if err != nil {
		return http.StatusBadGateway, "something was wrong about database", nil
	}
	defer sqlDB.Close()

	//NOTE check username has already exist
	var getUser []entities.Users
	db.Where("username = ?", reqLogin.Username).Find(&getUser)
	if len(getUser) <= 0 {
		return http.StatusNotFound, "can't found this user", nil
	} else if getUser[0].Type_account != accountType {
		return http.StatusForbidden, "account ain't admin", nil
	}

	return http.StatusOK, "", getUser
}

func StoreTokenUserLogin(username string, token string, refToken string) (int, string) {
	db := mysql.ConnectLocalhost()
	if db == nil {
		return http.StatusBadGateway, "can't connect to database"
	}
	sqlDB, err := db.DB()
	if err != nil {
		return http.StatusBadGateway, "something was wrong about database"
	}
	defer sqlDB.Close()

	tokenResponse := new(entities.JwtTokens)
	tokenResponse.Username = username
	tokenResponse.Token = token
	tokenResponse.RefreshToken = refToken

	result := db.Create(tokenResponse)
	if result.Error != nil {
		return http.StatusBadGateway, "something was wrong with create user method"
	}

	return http.StatusOK, ""
}

func LoginGetLastExistTokenChecker(username string) (int, string, map[string]string) {
	db := mysql.ConnectLocalhost()
	if db == nil {
		return http.StatusBadGateway, "can't connect to database", nil
	}
	sqlDB, err := db.DB()
	if err != nil {
		return http.StatusBadGateway, "something was wrong about database", nil
	}
	defer sqlDB.Close()

	tokenResponse := new(entities.JwtTokens)
	tokenResponse.Username = username

	result := db.Where("username = ?", username).First(tokenResponse)
	if result.Error != nil {
		return http.StatusOK, "", nil
	}
	previousToken := helper.ValidatePreviousToke(tokenResponse.Token)
	if previousToken != "" {
		tokenResponse := map[string]string{
			"token":         tokenResponse.Token,
			"refresh_token": tokenResponse.RefreshToken,
		}
		return http.StatusOK, "", tokenResponse
	}else{
		deleteTokens := new(entities.JwtTokens)
		result := db.Where("username = ?", username).Delete(deleteTokens)
		if result.Error != nil {
			return http.StatusBadGateway, "can't delete old token", nil
		}
	}

	return http.StatusOK, "", nil
}
