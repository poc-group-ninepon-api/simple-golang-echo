package tasks

import (
	"fmt"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/tjarratt/babble"
)

func TaskSample() {
	currentTime := time.Now()
	fmt.Println("YYYY-MM-DD HH:MM:ss : ", currentTime.Format("1989-16-12 18:00:00"))

	//NOTE setup task scheduler
	s := gocron.NewScheduler(time.UTC)
	s.Every(5).Seconds().Do(func() {
		babbler := babble.NewBabbler()
		fmt.Println("print: " + babbler.Babble())
	})

	//NOTE start task scheduler
	// s.StartAsync()
}
