package config

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func ConfigCors() middleware.CORSConfig {
	config := middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}

	return config
}
