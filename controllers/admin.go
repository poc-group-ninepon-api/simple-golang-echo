package controllers

import (
	"fmt"
	"net/http"

	"ninepon-golang-echo/helper"
	"ninepon-golang-echo/models"
	"ninepon-golang-echo/validators"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
)

func AdminLogin(c echo.Context) error {

	//NOTE validate data request
	v := validator.New()
	reqLogin := new(validators.Login)
	if err := c.Bind(&reqLogin); err != nil {
		return c.String(http.StatusBadGateway, "can't bind request data")
	}
	validateErr := v.Struct(reqLogin)
	if validateErr != nil {
		fmt.Println(validateErr)
		return c.String(http.StatusBadRequest, "bad request")
	}

	//NOTE connect to model
	statusHttp, err, getUser := models.GetDataUserLogin(reqLogin, "admin")
	if err != "" {
		return c.String(statusHttp, err)
	}

	//NOTE check match password
	match := helper.CheckPasswordHash(reqLogin.Password, getUser[0].Password)
	fmt.Println("CheckPasswordHash match: ", match)
	if match {

		//NOTE create data map for generate token
		userPwd := map[string]string{
			"username": reqLogin.Username,
			"password": reqLogin.Password,
			"deviceName": "",
			"platform": "",
		}

		//NOTE check previous token
		statusHttp, errMsg, oldToken := models.LoginGetLastExistTokenChecker(reqLogin.Username)
		if errMsg != "" {
			return c.String(statusHttp, errMsg)
		}
		if oldToken != nil {
			return c.JSON(http.StatusOK, oldToken)
		}

		//NOTE previous token have issue, generate new token
		tokenJWT := helper.GenerateJWT(userPwd)
		if len(tokenJWT) <= 0 {
			return c.NoContent(http.StatusInternalServerError)
		}
		refreshToken := helper.RandomUUID()
		statusHttp, StoreTokenUserLoginErr := models.StoreTokenUserLogin(reqLogin.Username, tokenJWT, refreshToken)
		if StoreTokenUserLoginErr != "" {
			return c.String(statusHttp, err)
		}
		tokenResponse := map[string]string{
			"token":         tokenJWT,
			"refresh_token": refreshToken,
		}

		return c.JSON(http.StatusOK, tokenResponse)
	} else {
		return c.String(http.StatusForbidden, "wrong password")
	}
}
