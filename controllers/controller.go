package controllers

import (
	"ninepon-golang-echo/services/firebase"
	pRedis "ninepon-golang-echo/services/redis"

	"github.com/go-redis/redis"
)

type UsernamePassword struct {
	Username string `json:"username" form:"username" query:"username"`
	Password string `json:"password" form:"password" query:"password"`
}

type tokenResponse struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
}

type InputRedisWrite struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

var firestore firebase.Firestore
var rdb *redis.Client

func firestoreConnect() {
	firestore = firebase.ConnectFirestore()
}

func redisConnect() {
	rdb = pRedis.ConnectPubsub()
}
