package controllers

import (
	"context"
	"fmt"
	"net/http"
	"reflect"

	"github.com/labstack/echo"
	"google.golang.org/api/iterator"
	"ninepon-golang-echo/helper"
	"ninepon-golang-echo/models"
	"ninepon-golang-echo/validators"

	"github.com/go-playground/validator/v10"
)

func UserLogin(c echo.Context) error {
	//NOTE validate data request
	v := validator.New()
	reqLogin := new(validators.Login)
	if err := c.Bind(&reqLogin); err != nil {
		return c.String(http.StatusBadGateway, "can't bind request data")
	}
	validateErr := v.Struct(reqLogin)
	if validateErr != nil {
		fmt.Println(validateErr)
		return c.String(http.StatusBadRequest, "bad request")
	}

	//NOTE connect to model
	statusHttp, err, getUser := models.GetDataUserLogin(reqLogin, "user")
	if err != "" {
		return c.String(statusHttp, err)
	}

	//NOTE check match password
	match := helper.CheckPasswordHash(reqLogin.Password, getUser[0].Password)
	fmt.Println("CheckPasswordHash match: ", match)
	if match {

		//NOTE create data map for generate token
		userPwd := map[string]string{
			"username": reqLogin.Username,
			"password": reqLogin.Password,
			"deviceName": "",
			"platform": "",
		}

		//NOTE check previous token
		statusHttp, errMsg, oldToken := models.LoginGetLastExistTokenChecker(reqLogin.Username)
		if errMsg != "" {
			return c.String(statusHttp, errMsg)
		}
		if oldToken != nil {
			return c.JSON(http.StatusOK, oldToken)
		}

		//NOTE previous token have issue, generate new token
		tokenJWT := helper.GenerateJWT(userPwd)
		if len(tokenJWT) <= 0 {
			return c.NoContent(http.StatusInternalServerError)
		}
		refreshToken := helper.RandomUUID()
		statusHttp, StoreTokenUserLoginErr := models.StoreTokenUserLogin(reqLogin.Username, tokenJWT, refreshToken)
		if StoreTokenUserLoginErr != "" {
			return c.String(statusHttp, err)
		}
		tokenResponse := map[string]string{
			"token":         tokenJWT,
			"refresh_token": refreshToken,
		}

		return c.JSON(http.StatusOK, tokenResponse)
	} else {
		return c.String(http.StatusForbidden, "wrong password")
	}
}

func UserShow(c echo.Context) error {
	firestoreConnect()
	defer firestore.Client.Close()
	username := c.Param("username")
	fmt.Println(username)
	doc, err := firestore.Client.Collection("users").Doc(username).Get(context.Background())
	if err != nil {
		fmt.Println("Error firestore")
		return c.String(http.StatusNotFound, "not found user data")
	}
	dataMap := doc.Data()
	fmt.Println("*********")
	fmt.Println(reflect.TypeOf(dataMap))
	fmt.Println("*********")
	return c.JSON(http.StatusOK, dataMap)
}

func UserIndex(c echo.Context) error {
	firestoreConnect()
	defer firestore.Client.Close()
	var arrDocIdUsername []string
	iter := firestore.Client.Collection("users").Documents(context.Background())
	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return err
		}
		arrDocIdUsername = append(arrDocIdUsername, doc.Ref.ID)
		fmt.Println(doc.Ref.ID)
		// fmt.Println(doc.Data())
	}
	if len(arrDocIdUsername) <= 0 {
		return c.String(http.StatusNotFound, "not found user data")
	} else {
		return c.JSON(http.StatusOK, arrDocIdUsername)
	}
}
