package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"ninepon-golang-echo/entities"
	"ninepon-golang-echo/helper"
	pMysql "ninepon-golang-echo/services/mysql"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
)

var users []entities.Users

func MySQLIndex(c echo.Context) error {
	//NOTE example route path: /mysql?perPage=3&currentPage=2
	mysql := pMysql.ConnectLocalhost()
	if mysql == nil {
		return c.String(http.StatusBadGateway, "can't connect to database")
	}
	sqlDB, err := mysql.DB()
	if err != nil {
		return c.String(http.StatusBadGateway, "something was wrong about database")
	}
	defer sqlDB.Close()

	//NOTE get all data for calculate total
	var countRowInCondition int64
	mysql.Find(&users).Count(&countRowInCondition)
	var getAllUsers []entities.Users
	mapPagination := helper.SetByParamQuery(c.QueryParams())

	if mapPagination["perPage"] == 0 {
		//NOTE query condition pagination
		mysql.Omit("password").Find(&users)
		mapPagination["perPage"] = len(users)
	} else {
		//NOTE query condition pagination
		mysql.Omit("password").Find(&getAllUsers).Offset(mapPagination["offset"]).Limit(mapPagination["perPage"]).Find(&users)
	}
	getAllUsers = users

	res := helper.ResponseWithPagination(
		int(countRowInCondition),
		mapPagination["currentPage"],
		mapPagination["perPage"],
		getAllUsers,
	)

	return c.JSON(http.StatusOK, res)
}

func MySQLShow(c echo.Context) error {
	mysql := pMysql.ConnectLocalhost()
	if mysql == nil {
		return c.String(http.StatusBadGateway, "can't connect to database")
	}
	sqlDB, err := mysql.DB()
	if err != nil {
		return c.String(http.StatusBadGateway, "something was wrong about database")
	}
	defer sqlDB.Close()
	userIcode := c.Param("userIcode")

	params := c.QueryParams()
	currentPage, currentPageErr := strconv.Atoi(params.Get("currentPage"))
	perPage, perPageErr := strconv.Atoi(params.Get("perPage"))
	var offset int
	
	var enitityShowUser []entities.Users
	queyCmd := mysql.Where("idcode = ?", userIcode).Omit("password")
	if currentPageErr != nil || perPageErr != nil {
		fmt.Println("ERROR ABOUT PAGINATION")
		queyCmd.Find(&enitityShowUser)
	} else {
		offset = (currentPage - 1) * perPage
		queyCmd.Find(&enitityShowUser).Offset(offset).Limit(perPage).Find(&enitityShowUser)
	}
	for _, user := range enitityShowUser {
		fmt.Println(user.Fullname, user.Tel)
	}

	return c.JSON(http.StatusOK, enitityShowUser)
}

func MysqlStore(c echo.Context) error {
	v := validator.New()
	mysql := pMysql.ConnectLocalhost()
	if mysql == nil {
		return c.String(http.StatusBadGateway, "can't connect to database")
	}
	sqlDB, err := mysql.DB()
	if err != nil {
		return c.String(http.StatusBadGateway, "something was wrong about database")
	}
	defer sqlDB.Close()

	//NOTE validate request data
	newUser := new(entities.Users)
	if err := c.Bind(&newUser); err != nil {
		return c.String(http.StatusBadGateway, "can't bind request data")
	}
	vErr := v.Struct(newUser)
	if vErr != nil || (len(newUser.Username) == 0 && len(newUser.Password) == 0) {
		fmt.Println(vErr)
		return c.String(http.StatusBadRequest, "bad request")
	}

	//NOTE hashing password
	passw, hpErr := helper.HashPassword(newUser.Password)
	if hpErr != nil {
		return c.String(http.StatusBadGateway, "can't hash password")
	}
	newUser.Password = passw

	//NOTE check has duplicate data
	var findDuplicateUser []entities.Users
	mysql.Where("idcode = ?", newUser.IDcode).Find(&findDuplicateUser)
	if len(findDuplicateUser) > 0 {
		return c.String(http.StatusUnprocessableEntity, "duplicate data")
	}

	//NOTE create user
	result := mysql.Create(&newUser)
	if result.Error != nil {
		fmt.Println(result.Error)
		return c.String(http.StatusBadGateway, "something was wrong with create user method")
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "created user has successful",
	})
}

func MysqlDelete(c echo.Context) error {
	mysql := pMysql.ConnectLocalhost()
	if mysql == nil {
		return c.String(http.StatusBadGateway, "can't connect to database")
	}
	sqlDB, err := mysql.DB()
	if err != nil {
		return c.String(http.StatusBadGateway, "something was wrong about database")
	}
	defer sqlDB.Close()
	userIcode := c.Param("userIcode")

	//NOTE check exist data as while deleting
	var findDuplicateUser []entities.Users
	mysql.Where("idcode = ?", userIcode).Find(&findDuplicateUser)
	if len(findDuplicateUser) <= 0 {
		return c.String(http.StatusNotFound, "data "+userIcode+" hasn't found")
	}

	//NOTE delete user
	result := mysql.Where("idcode = ?", userIcode).Delete(&users)
	if result.Error != nil {
		return c.String(http.StatusBadGateway, "something was wrong with delete user method")
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "deleted user has successful",
	})
}

func MysqlUpdate(c echo.Context) error {
	v := validator.New()
	mysql := pMysql.ConnectLocalhost()
	if mysql == nil {
		return c.String(http.StatusBadGateway, "can't connect to database")
	}
	sqlDB, err := mysql.DB()
	if err != nil {
		return c.String(http.StatusBadGateway, "something was wrong about database")
	}
	defer sqlDB.Close()
	userIcode := c.Param("userIcode")

	//NOTE check exist data as while updating
	var findDuplicateUser []entities.Users
	mysql.Where("idcode = ?", userIcode).Find(&findDuplicateUser)
	if len(findDuplicateUser) <= 0 {
		return c.String(http.StatusNotFound, "data "+userIcode+" hasn't found")
	}

	//NOTE validate request data
	newUser := new(entities.Users)
	if err := c.Bind(&newUser); err != nil {
		return c.String(http.StatusBadGateway, "can't bind request data")
	}
	vErr := v.Struct(newUser)
	if vErr != nil {
		fmt.Println(vErr)
		return c.String(http.StatusBadRequest, "bad request")
	}

	//NOTE update user
	result := mysql.Where("idcode = ?", userIcode).Updates(&newUser)
	if result.Error != nil {
		return c.String(http.StatusBadGateway, "something was wrong with update user method")
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"message": "updated user has successful",
	})
}
