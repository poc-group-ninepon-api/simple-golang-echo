package controllers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
)

func ReadRedisKey(c echo.Context) error {
	redisConnect()
	defer rdb.Close()
	key := c.Param("key")
	if key == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "key is required")
	}
	val, err := rdb.Get(key).Result()
	if err != nil {
		return c.JSON(http.StatusNotImplemented, "[error] redis error")
	}
	return c.JSON(http.StatusOK, val)
}

func WriteRedisKey(c echo.Context) error {
	redisConnect()
	defer rdb.Close()
	inputRequestJSON := new(InputRedisWrite)
	if err := c.Bind(inputRequestJSON); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}
	err := rdb.Set(inputRequestJSON.Key, inputRequestJSON.Value, 0).Err()
	if err != nil {
		fmt.Println("ERROR")
		fmt.Println(err)
		// return c.String(http., "ok!")
	} else {
		fmt.Println("Redis write success")
	}
	return c.JSON(http.StatusOK, "ok!")
}
